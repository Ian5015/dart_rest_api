import 'dart:async';
import 'dart:io';

import 'package:logging/logging.dart';
import 'package:logging_handlers/server_logging_handlers.dart';
import 'package:rpc/rpc.dart';
import 'package:shelf_rpc/shelf_rpc.dart' as shelf_rpc;
import 'package:shelf/shelf.dart' as shelf;
import 'package:shelf/shelf_io.dart' as shelf_io;
import 'package:shelf_route/shelf_route.dart' as shelf_route;
import 'package:shelf_web_socket/shelf_web_socket.dart';
import 'package:redstone_mapper/mapper_factory.dart';

import 'package:dart_rest_api/config/config.dart';
import 'package:dart_rest_api/api/user_api.dart';
import 'package:dart_rest_api/logic/user/user_logic.dart';
//import 'package:dart_rest_api/api/socket.dart';

const String _apiPrefix = '/api';
final ApiServer _apiServer =
    new ApiServer(apiPrefix: _apiPrefix, prettyPrint: true);

Future main() async {
  new AppConfig();
  bootstrapMapper();

  // Add a simple log handler to log information to a server side file.
  Logger.root.level = Level.ALL;
  Logger.root.onRecord.listen(new SyncFileLoggingHandler('myLogFile.txt'));
  if (stdout.hasTerminal) {
    Logger.root.onRecord.listen(new LogPrintHandler());
  }

  _apiServer.addApi(new UserApi());

  // Create a Shelf handler for your RPC API.
  final apiHandler = shelf_rpc.createRpcHandler(_apiServer);

  webSocketHandler((ws) {
    ws.listen((message) {
      print(message);
    });
  });

//  var wsHandler = webSocketHandler((webSocket) {
//    webSocket.listen((message) {
//      webSocket.add("echo $message");
//    });
//  });

  final wsHandler = webSocketHandler(apiHandler);

  final apiRouter = shelf_route.router();
  apiRouter.add(_apiPrefix, null, apiHandler, exactMatch: false);
  apiRouter.add('/ws', null, wsHandler, exactMatch: false);

  final handler = const shelf.Pipeline()
      .addMiddleware(shelf.logRequests())
      .addHandler(apiRouter.handler);

  final Map<String, String> envVars = Platform.environment;
  final int port = int.parse(envVars['PORT'] ?? 8080.toString());
  final server = await shelf_io.serve(handler, '0.0.0.0', port);

  print('HTTP server listening at port ${server.port}.');
}
