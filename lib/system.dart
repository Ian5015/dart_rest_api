library system;

class Payload<T> {
  List<ErrorResponse> errors = new List<ErrorResponse>();
  T data;
  bool isSuccess;

  Payload();

  Payload.init(T d) {
    this.data = d;
  }

  addError(ErrorResponse error) {
    isSuccess = false;
    errors.add(error);
  }
}

class ErrorResponse {
  String message;
  String fieldName;

  ErrorResponse(String message, [String fieldName]) {
    this.message = message;
    this.fieldName = fieldName;
  }
}
