library base_logic;

import 'package:dart_rest_api/system.dart';
import 'package:dart_rest_api/logic/errors2.dart';

abstract class BaseLogic {
  ValidateRequired(
      Payload payload, String data, String fieldName, String displayName) {
    if (data == null || data == '') {
      payload.errors.add(new ErrorResponse(
          new Errors().Error('00001', displayName), fieldName));
    }
  }
}
