library errors;

class Errors {
  static final Error_00001 = '{0} is required.';

  /// Error allows passage of a code to match a string in the errors map.
  /// Can currently pass up to three variables to be used in string interpolation.
  String Error(String error, [String a, String b, String c]) {

    if (a != null && a != '') {
      error.replaceAll(new RegExp('{0}'), a);
    }

    if (b != null && b != '') {
      error.replaceAll(new RegExp('{1}'), a);
    }

    if (c != null && c != '') {
      error.replaceAll(new RegExp('{2}'), c);
    }

    return error;
  }
}
