library user_logic;

import 'dart:async';

import 'package:dbcrypt/dbcrypt.dart';
import 'package:dart_jwt/dart_jwt.dart';

import 'package:dart_rest_api/system.dart';
import 'package:dart_rest_api/logic/base_logic.dart';
import 'package:dart_rest_api/logic/errors2.dart';
import 'package:dart_rest_api/models/user/user.dart';
import 'package:dart_rest_api/models/filters/user_filter_criteria.dart';
import 'package:dart_rest_api/repository/user/user_repository.dart';



class AuthLogic extends BaseLogic {
  final _userRepository = new UserRepository();
  final _errors = new Errors();
  final _dbcrypt = new DBCrypt();
  String signKey;
  String verifyKey;
  Map<String, String> _signers;

  ///
  AuthLogic() {
    final path = Platform.script.resolve('keys').toFilePath();
    final signFile = new File(path + '/app.rsa');
    final verifyFile = new File(path + '/app.rsa.pub');
    signKey = signFile.readAsStringSync();
    verifyKey = verifyFile.readAsStringSync();
  }

  ///
  String authenticateToken(String tokenString) {
    return 'string';
  }

  /// Get a list of users that match the filter criteria.
  Future<Payload<String>> login(String email, String password) async {
    var token = '';
    // Get user.
    final users = await _userRepository
        .Get(new UserFilterCriteria()..email = email);
    final user = users.first;

    if (user == null) {
       // Invalid email or password error.
    }

    final validPassword = verifyPassword(user, password);

    if (validPassword) {

    }

    final payload = new Payload<List<String>>();
    payload.data.add('string');
    return new Future.value(payload);
  }

  ///
  bool verifyPassword(User user, String password) {
    final saltedPassword = user.password + user.salt;
    return _dbcrypt.checkpw(saltedPassword, user.password);
  }
}
