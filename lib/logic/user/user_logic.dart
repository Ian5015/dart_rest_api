library user_logic;

import 'dart:async';
import 'package:dart_rest_api/system.dart';
import 'package:dart_rest_api/logic/base_logic.dart';
import 'package:dart_rest_api/logic/errors2.dart';
import 'package:dart_rest_api/models/user/user.dart';
import 'package:dart_rest_api/models/filters/user_filter_criteria.dart';
import 'package:dart_rest_api/repository/user/user_repository.dart';

class UserLogic extends BaseLogic {
  final _userRepository = new UserRepository();
  final _errors = new Errors();

  /// Get a list of users that match the filter criteria.
  Future<Payload<List<UserDetailDTO>>> Get(UserFilterCriteria filter) async {
    var payload = new Payload<List<UserDetailDTO>>();
    var userDetailDTOs = new List<UserDetailDTO>();
    var users = await _userRepository.Get(filter);

    users.forEach(
        (u) => userDetailDTOs.add(new UserDetailDTO.fromDomainModel(u)));

    payload.data = userDetailDTOs;
    return payload;
  }

  /// Get a single user based on an id.
  Future<Payload<UserDetailDTO>> GetSingle(String id) async {
    var payload = new Payload<UserDetailDTO>();
    var user = await _userRepository.GetSingle(id);
    UserDetailDTO userDetailDTO;

    if (user == null) {
      payload.addError(new ErrorResponse('No matching user was found.'));
    } else {
      userDetailDTO = new UserDetailDTO.fromDomainModel(user);
    }

    payload.data = userDetailDTO;
    return new Future.value(payload);
  }

  /// Create a user.
  Future<Payload<UserDetailDTO>> Create(UserInputDTO dto) async {
    var userToCreate = new User.fromInputDTO(dto);
    userToCreate.setCreateStamps();
    await _userRepository.Create(userToCreate);
    var pld = await GetSingle(userToCreate.id);
    var userDetailDTO = pld.data;
    var payload = new Payload<UserDetailDTO>.init(userDetailDTO);
    return new Future.value(payload);
  }

  /// Delete a user.
  Future<Payload<bool>> Delete(String id) async {
    var payload = new Payload<bool>();
    bool success = await _userRepository.Delete(id);

    if (success) {
      payload.addError(new ErrorResponse('No matching user was found to delete.'));
    }

    payload.data = success;
    return new Future.value(payload);
  }

  /// Update a user.
  Future<Payload<UserDetailDTO>> Update(UserInputDTO dto) async {
    var userToUpdate = new User.fromInputDTO(dto);
    userToUpdate.setUpdateStamps();
    await _userRepository.Update(userToUpdate);
    var pld = await GetSingle(userToUpdate.id);
    var userDetailDTO = pld.data;
    var payload = new Payload<UserDetailDTO>.init(userDetailDTO);
    return new Future.value(payload);
  }

  /// General validation for all creates and updates.
  void Validate(Payload payload, User user) {
    if (user.id == null || user.id == '') {
      payload.addError(new ErrorResponse(_errors.Error('00001', '"id"'), 'id'));
    }
    if (user.firstName == null || user.firstName == '') {
      payload.addError(new ErrorResponse(
          _errors.Error('00001', '"First name"'), 'firstName'));
    }
    if (user.lastName == null || user.lastName == '') {
      payload.addError(
          new ErrorResponse(_errors.Error('00001', '"Last name"'), 'lastName'));
    }
  }
}
