library errors;

class Errors {
  static final Errors _errors = new Errors._internal();

  factory Errors() {
    return _errors;
  }

  Map<String, String> get map => _map;

  /// Error allows passage of a code to match a string in the errors map.
  /// Can currently pass up to three variables to be used in string interpolation.
  String Error(String code, [String a, String b, String c]) {
    var error = _map[code];

    if (a != null && a != '') {
      error.replaceAll(new RegExp('{0}'), a);
    }

    if (b != null && b != '') {
      error.replaceAll(new RegExp('{1}'), a);
    }

    if (c != null && c != '') {
      error.replaceAll(new RegExp('{2}'), c);
    }

    return error;
  }

  Errors._internal();

  Map<String, String> _map = {'00001': '{0} is required.'};
}
