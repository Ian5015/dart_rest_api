// Copyright (c) 2014, the Dart project authors.  Please see the AUTHORS file
// for details. All rights reserved. Use of this source code is governed by a
// BSD-style license that can be found in the LICENSE file.

library user_api;

import 'dart:async';

import 'package:rpc/rpc.dart';

import 'package:dart_rest_api/system.dart';
import 'package:dart_rest_api/logic/user/user_logic.dart';
import 'package:dart_rest_api/models/user/user.dart';
import 'package:dart_rest_api/models/filters/user_filter_criteria.dart';

@ApiClass(version: '0.1')
class UserApi {
  var testUser = new User();

  var _userLogic = new UserLogic();

  UserApi();

  @ApiMethod(path: 'get/{id}')
  Future<Payload<UserDetailDTO>> getSingle(String id) async {
    return await _userLogic.GetSingle(id);
  }

  @ApiMethod(path: 'get', method: 'POST')
  Future<Payload<List<UserDetailDTO>>> get(UserFilterCriteria filter) async {
    return await _userLogic.Get(filter);
  }

  @ApiMethod(path: 'create', method: 'POST')
  Future<Payload<UserDetailDTO>> create(UserInputDTO input) async {
    return await _userLogic.Create(input);
  }

  @ApiMethod(path: 'update', method: 'POST')
  Future<Payload<UserDetailDTO>> update(UserInputDTO input) async {
    return await _userLogic.Update(input);
  }
}
