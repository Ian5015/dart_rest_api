library db_helper;

import 'dart:async';

import 'package:redstone_mapper_mongo/manager.dart';

import 'package:dart_rest_api/config/config.dart';

class DbHelper {
  static final DbHelper _dbHelper = new DbHelper._internal();
  static final _appConfig = new AppConfig();

  factory DbHelper() {
    return _dbHelper;
  }

  Future<MongoDb> GetConnection() async {
    var dbManager =
        await new MongoDbManager(_appConfig.DbConnectionStringWithAuth);
    var conn = await dbManager.getConnection();
    return conn;
  }

  MongoDbManager dbManager() {
    return new MongoDbManager(_appConfig.DbConnectionStringWithAuth);
  }

  DbHelper._internal();
}
