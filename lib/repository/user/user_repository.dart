library user_repository;

import 'package:dart_rest_api/models/user/user.dart';
import 'package:dart_rest_api/models/filters/user_filter_criteria.dart';
import 'package:dart_rest_api/repository/base_repository.dart';

class UserRepository extends BaseRepository {
  String _collectionName = 'users';
  Future<MongoDb> get _connection async {
    var conn = await new DbHelper().GetConnection();
    //conn.innerConn.ensureIndex(_collectionName, key: 'email');
    return conn;
  }

  /// Creates a user from a User [input].
  Future Create(@Decode() User input) async {
    var conn = await _connection;
    await conn.insert(_collectionName, input);
  }

  /// Deletes a user that has [id].
  Future<bool> Delete(String id) async {
    var conn = await _connection;
    // Result is a WriteResult object from MongoDB.
    // See: https://docs.mongodb.com/manual/reference/method/db.collection.remove/#behavior
    var result = await conn.remove(_collectionName, new SelectorBuilder().id(new ObjectId.fromHexString(id)));
    // `n` is the number of documents found via the above query.
    var success = result['n'] > 0;
    return success;
  }

  /// Gets users that match the [filter] criteria.
  @Encode()
  Future<List<User>> Get(UserFilterCriteria filter) async {
    var query = baseQuery(filter);

    if (filter.id != '') {
      query = query.id(ObjectId.parse(filter.id));
    }
    if (filter.ids != null && filter.ids.length > 0) {
      var _ids = new List<ObjectId>();
      filter.ids.forEach((id) => _ids.add(ObjectId.parse(id)));
      query = query.all('_id', _ids);
    }
    if (filter.firstName != '') {
      query = query.eq('firstname', filter.firstName);
    }
    if (filter.lastName != '') {
      query = query.eq('lastname', filter.lastName);
    }
    if (filter.username != '') {
      query = query.eq('username', filter.username);
    }
    if (filter.email != '') {
      query = query.eq('email', filter.email);
    }

    // Sorting
    if (filter.sortProperty != '') {
      query = query.sortBy(filter.sortProperty, descending: filter.descending);
    } else {
      query = query.sortBy(UserSortProperties.LastName,
          descending: filter.descending);
    }

    // Get the users
    var conn = await _connection;
    var users = await conn.find(_collectionName, User, query);

    return new Future.value(users);
  }

  /// Gets a single user that has [id].
  @Encode()
  Future<User> GetSingle(String id) async {
    var user = await _connection.then(
        (c) => c.findOne(_collectionName, User, where.id(ObjectId.parse(id))));
    return new Future.value(user);
  }

  /// Updates a user given a User [input].
  Future Update(@Decode() User input) async {
    var conn = await _connection;
    await conn.update(
        _collectionName, where.eq('username', input.username), input,
        override: false);
  }

  /// Takes a [filter] and builds an initial query.
  SelectorBuilder baseQuery(BaseFilterCriteria filter) {
    return new SelectorBuilder().limit(filter.pageSize).skip(filter.skip);
  }
}
