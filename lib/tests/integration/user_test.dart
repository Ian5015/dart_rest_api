import 'package:test/test.dart';
import 'package:mongo_dart/mongo_dart.dart';
import 'package:redstone_mapper/mapper_factory.dart';
import 'package:redstone_mapper/plugin.dart';

import 'package:dart_rest_api/config/config.dart';
import 'package:dart_rest_api/models/user/user.dart';
import 'package:dart_rest_api/logic/user/user_logic.dart';
import 'package:dart_rest_api/repository/db_helper.dart';

String _testId = '57251c5307358e495a8058f8';
String _testCreateId = '5725296c0c24449ef1ea90f2';

UserLogic _logic = new UserLogic();

User _testUser = new User()
  ..id = _testId
  ..username = 'testusername'
  ..firstName = 'Lori'
  ..lastName = 'Munnerley'
  ..email = 'test@email.com'
  ..created = new DateTime.now()
  ..lastUpdated = new DateTime.now();

main() async {
  new AppConfig();
  bootstrapMapper();
  var conn = await new DbHelper().GetConnection();

  setUpFunc(@Decode() User user) async {
    //var conn = await _connection;
    await conn.insert('users', user);
  }

  await setUp(() async {
    // Create test data and store on db.
    setUpFunc(_testUser);
  });

  await tearDown(() async {
    // Delete all test data from the db.
    //var conn = await _connection;
    await conn.remove('users', where.id(ObjectId.parse(_testId)));
    await conn.remove('users', where.id(ObjectId.parse(_testCreateId)));
  });

  // get test user
  test('UserLogic.GetSingle() returns a single user', () async {
    // Get a user by id.
    var pld = await _logic.GetSingle(_testId);
    expect(pld.data, isNotNull);
    expect(pld.errors.length, isZero);
  });

  // create user
  test('UserLogic.Create() creates a user', () async {
    var inputDTO = new UserInputDTO()
      ..id = _testCreateId
      ..firstName = 'firstname'
      ..lastName = 'lastname'
      ..email = 'test2@email.com'
      ..username = 'testusername2';

    // Create the user.
    await _logic.Create(inputDTO);

    // Retrieve the created user.
    //var conn = await _connection;
    var userMap = await conn
        .collection('users')
        .find(where.eq('username', 'testusername2').limit(1));
    var user = await userMap.first;

    expect(user, isNotNull);
    expect(user['firstname'], inputDTO.firstName);
    expect(user['lastname'], inputDTO.lastName);
    //expect(user['_id'], inputDTO.id);
  });

  // update user
  // create user
  test('UserLogic.Update() updates the test user', () async {
    var inputDTO = new UserInputDTO()
      ..id = _testId
      ..firstName = 'Astor'
      ..lastName = _testUser.lastName
      ..email = _testUser.email
      ..username = _testUser.username;

    // Update the user.
    await _logic.Update(inputDTO);

    // Retrieve the user.
    var pld = await _logic.GetSingle(_testId);
    var user = pld.data;

    expect(user.firstName, 'Astor');
  });

  // delete user
  test('UserLogic.Delete() disables user', () async {
    // Positive test.
    var pld = await _logic.Delete(_testId);
    expect(pld.data, true);

    // Negative test.
    pld = await _logic.Delete(new ObjectId().toHexString());
    expect(pld.data, false);
  });
}
