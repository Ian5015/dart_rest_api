part of user_model;

class UserDetailDTO {
  String id;
  String firstName;
  String lastName;
  String email;
  String username;
  String fullName;

  UserDetailDTO();

  UserDetailDTO.fromDomainModel(User user) {
    if (user != null) {
      this.id = user.id;
      this.firstName = user.firstName;
      this.lastName = user.lastName;
      this.email = user.email;
      this.username = user.username;

      // just using this to initialize its value from the getter until I figure
      // figure out a cleaner way.
      this.fullName = '$firstName $lastName';
    } else {
      throw new Exception('User not passed to constructor!');
    }
  }
}
