library user_model;

import 'package:redstone_mapper/mapper.dart';
import 'package:dart_rest_api/models/base.dart';

part 'user_input_dto.dart';
part 'user_detail_dto.dart';

class User extends BaseModel {
  @Field(view: 'firstname', model: 'firstname')
  String firstName;

  @Field(view: 'lastname', model: 'lastname')
  String lastName;

  @Field()
  String email;

  @Field()
  String username;

  @Field()
  String password;

  @Field()
  String salt;

  User();

  User.fromInputDTO(UserInputDTO input) {
    if (input.id != null && input.id != '') {
      this.id = input.id;
    }

    this.firstName = input.firstName;
    this.lastName = input.lastName;
    this.email = input.email;
    this.username = input.username;
  }

  User.fromMap(Map map) {
    if (map.containsKey('_id')) {
      this.id = map['_id'];
    }
    if (map.containsKey('firstname')) {
      this.firstName = map['firstname'];
    }
    if (map.containsKey('lastname')) {
      this.lastName = map['lastname'];
    }
    if (map.containsKey('email')) {
      this.email = map['email'];
    }
    if (map.containsKey('username')) {
      this.username = map['username'];
    }
    if (map.containsKey('password')) {
      this.password = map['password'];
    }
    if (map.containsKey('salt')) {
      this.salt = map['salt'];
    }
    if (map.containsKey('created')) {
      this.created = map['created'];
    }
    if (map.containsKey('lastupdated')) {
      this.lastUpdated = map['lastupdated'];
    }
  }
}
