part of user_model;

class UserInputDTO {
  String id;
  String firstName;
  String lastName;
  String email;
  String username;
}
