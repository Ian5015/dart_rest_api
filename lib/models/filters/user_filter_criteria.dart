library user_filter_criteria;

import 'package:dart_rest_api/models/filters/base_filter_criteria.dart';

class UserSortProperties {
  static final Username = 'username';
  static final FirstName = 'firstname';
  static final LastName = 'lastname';
  static final Email = 'email';
  static final Created = 'created';
  static final LastUpdated = 'lastupdated';
}

class UserFilterCriteria extends BaseFilterCriteria {
  String id;
  List<String> ids;
  String firstName;
  String lastName;
  String email;
  String username;
  bool includeInactive;
}
