library base_filter_criteria;

import 'package:dart_rest_api/config/config.dart';

class BaseFilterCriteria {
  int pageIndex;
  int pageSize;
  String sortProperty;
  bool descending;

  int get skip => pageIndex * pageSize;

  BaseFilterCriteria() {
    this.pageIndex = this.pageIndex ?? 0;
    this.pageSize = this.pageSize ?? new AppConfig().DefaultPageSize;
    this.descending = this.descending ?? false;
  }
}
