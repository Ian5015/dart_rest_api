library base_model;

import 'package:mongo_dart/mongo_dart.dart';
import 'package:redstone_mapper/mapper.dart';
import 'package:redstone_mapper_mongo/metadata.dart';

class BaseModel {
  @Id()
  String id;

  @Field()
  DateTime created;

  @Field(view: 'lastupdated', model: 'lastupdated')
  DateTime lastUpdated;

  setCreateStamps() {
    this.id = this.id ?? new ObjectId().toHexString();
    this.created = this.created ?? new DateTime.now();
    this.lastUpdated = this.lastUpdated ?? new DateTime.now();
  }

  setUpdateStamps() {
    this.lastUpdated = new DateTime.now();
  }
}
