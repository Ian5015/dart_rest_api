library config;

import 'dart:io';
import 'dart:convert';

class AppConfig {
  static final AppConfig _appConfig = new AppConfig._internal();
  static final _env = Platform.environment;
  static final _defaultPageSize = 25;
  static String _dbConnectionStringWithAuth;
  static String _dbConnectionString;
  static String _dbUsername;
  static String _dbPassword;
  static String _signKey;
  static String _verifyKey;

  int get DefaultPageSize => _defaultPageSize;
  String get DbConnectionStringWithAuth => _dbConnectionStringWithAuth;
  String get DbConnectionString => _dbConnectionString;
  String get DbUsername => _dbUsername;
  String get DbPassword => _dbPassword;
  String get SignKey => _signKey;
  String get VerifyKey => _verifyKey;

  ///
  factory AppConfig() {
    _dbConnectionStringWithAuth = _env['DART_API_DB_CONNECTION_WITH_AUTH'];
      _dbConnectionString = _env['DART_API_DB_CONNECTION'];
    _dbUsername = _env['DART_API_DB_USERNAME'];
    _dbPassword = _env['DART_API_DB_PASSWORD'];
    final path = Platform.script.resolve('keys').toFilePath();
    final signFile = new File(path + '/app.rsa');
    final verifyFile = new File(path + '/app.rsa.pub');
    signKey = signFile.readAsStringSync();
    verifyKey = verifyFile.readAsStringSync();

    return _appConfig;
  }

  InitAppConfig(
      String dbConnectionString, String dbUsername, String dbPassword) {
    if (_dbConnectionString == null) {
      _dbConnectionString = dbConnectionString;
    }
    if (_dbUsername == null) {
      _dbUsername = dbUsername;
    }
    if (_dbPassword == null) {
      _dbPassword = dbPassword;
    }
    return _appConfig;
  }

  AppConfig._internal();
}
